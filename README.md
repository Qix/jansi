# JANSI

JANSI is a simple abstraction of the ANSI library that adds user-readable
function names to ANSI escape codes.

Further, JANSI allows strings to be color-formatted according to the
[Minecraft color scale](http://minecraft.gamepedia.com/Formatting_codes#Color_codes)
using `$` as a delimiter.

> The reason behind this is that the list is already there, it makes sense,
> and it's a really easy way to throw color into a string.

### Usage

```javascript
var jansi = require('jansi');
var $ = jansi.$;

process.stdout.write('Hello!');
jansi.left(6);
process.stdout.write('J');
jansi.right(6);
process.stdout.write('\n');

console.log($('$!Hello, $AW$Bo$Ar$Bl$Ad$!!'));
```