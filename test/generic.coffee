# Generic Testing
jansi = require '../lib/jansi/jansi.js'

# Raw write
_ = process.stdout.write.bind process.stdout

# Clear!
_ 'Hello!'
jansi.clear()
jansi.leftAbs()
_ 'Hello!'
jansi.clear 'before'
jansi.leftAbs()
_ 'Hello!'
jansi.leftAbs()
jansi.clear 'after'

# Jello!
_ 'Hello!'
jansi.left 6
_ 'J'
jansi.right 6
_ "\n"

_ 'Hello!'
jansi.leftAbs()
_ 'J'
jansi.clearLine 'after'
_ 'ello!'
_ "\n"

_ 'Hello!'
_ "\n"
jansi.up()
_ 'J'
jansi.down()
jansi.left()

_ "Hello!\nHello!\n"
jansi.upl 1
_ 'J'
jansi.upl 1
_ 'J'
jansi.downl 2

jansi.save()
_ 'Hello!'
jansi.restore()
_ 'J'
jansi.right 6
_ '\n'