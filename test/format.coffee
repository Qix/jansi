# Formatting test
jansi = require '../lib/jansi/jansi.js'
$ = jansi.$

console.log $ "$#{i}#{i}" for i in [0..9]
console.log $ "$#{i}#{i}" for i in ['A', 'B', 'C', 'D', 'E', 'F']

console.log $ "$!#{i}#{i}" for i in [0..9]
console.log $ "$!#{i}#{i}" for i in ['A', 'B', 'C', 'D', 'E', 'F']

console.log $ "$!Hello, $AW$Bo$Ar$Bl$Ad$!!"

console.log $ "You have $C$$-5014.75$! in savings!"