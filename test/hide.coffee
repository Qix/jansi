# Hide the cursor
jansi = require '../lib/jansi/jansi.js'

jansi.hide()
console.log "The cursor should be hidden (for 3 seconds)..."
setTimeout (()->), 3000