#
# JANSI
#	A better ANSI
#

# Bind raw writing
_ = process.stdout.write.bind process.stdout

# Setup escape code
_$ = (s) -> "\x1B[#{s}"
_$$ = (c, s) -> _$ s+c

# Writing helpers
$ = (s) -> _ _$ s
$$ = (c, s) -> _ _$$ c, s

# N Helper
N = (code, n = 1) -> (nn = n) -> $ "#{nn}#{code}"

# Codes
# Yes, Minecraft.
colorCodes =
	'0': [0, no]
	'1': [4, no]
	'2': [2, no]
	'3': [6, no]
	'4': [1, no]
	'5': [5, no]
	'6': [3, no]
	'7': [7, no]
	'8': [0, yes]
	'9': [4, yes]
	'A': [2, yes]
	'B': [6, yes]
	'C': [1, yes]
	'D': [5, yes]
	'E': [3, yes]
	'F': [7, yes]

# JANSI Object
J = 
	# CSI Codes
	up: N 'A'
	down: N 'B'
	right: N 'C'
	left: N 'D'
	downl: N 'E'
	upl: N 'F'
	leftAbs: N 'G', 0
	pos: (x=1, y=1) -> $ "#{x};#{y}H"
	clear: (mode = 'all') ->
		$$ 'J', switch mode
			when 'all' then 2
			when 'before' then 1
			when 'after' then 0
	clearLine: (mode = 'all') ->
		$$ 'K', switch mode
			when 'all' then 2
			when 'before' then 1
			when 'after' then 0
	scrollUp: N 'S'
	scrollDown: N 'T'
	save: N 's'
	restore: N 'u'
	hide: () -> $ '?25l' # hide() and show() will only be supported
	show: () -> $ '?25h' #	on *nix/VT100 compliant terminals until
						 #  https://github.com/joyent/libuv/pull/1262
						 #	is accepted and libuv is merged into
						 #	node, which will enable these on Windows. :)

	# Formatting Helpers
	$: (strs...) ->
		# Join + Replace
		strs.join ' '
			.replace /\$(\$|(?:(\!)?([0-9A-F])?))/g
				, (m, c, bg = false, code) ->
					if not code
						if bg then return _$ '0m'
						else return '$'
					color = colorCodes[code]
					additive = (if bg then 40 else 30)
					additive += 60 if color[1]
					colorCode = color[0] + additive
					return _$$ 'm', colorCode

	plain: (str) ->
		# Remove all
		str.replace /\x1B([NO\^_P\]]|(\[\??(\d+;?)*[A-HJKSTfmnsulh]))/g, ''




# Export
module.exports = J